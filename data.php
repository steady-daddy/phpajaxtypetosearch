<?php

require_once 'connection.php';

function getData($db, $city) {

	$query = "SELECT a.name as city,
					b.name as state,
					c.name as country
					FROM cities a JOIN
					states b ON
					a.state_id = b.id	JOIN
					countries c ON
					b.country_id = c.id
					WHERE a.name LIKE ? LIMIT 5";

	$param = array("$city%");
	$stmt = $db->prepare($query);
	$stmt->execute($param);
	return $stmt->fetchAll();
}

try {
	if(isset($_GET['city'])){

		$city = strip_tags($_GET['city']);
		$result = getData($pdo,$city);
		echo json_encode($result);
		exit;
	}

} catch(Exception $ex) {
	echo -1;
	exit;
}