(function (window) {
    'use strict';

    var Http = function() {
        /**
         * Helper for http calls
         * @param method
         * @param url
         * @param data
         * @returns {Promise}
         */
        function makeRequest(method,url,data) {
            var data = data || '';
            // Return a new promise.
            return new Promise(function(resolve, reject) {
                var req = new XMLHttpRequest();
                req.open(method, url);

                req.onload = function() {
                    if (req.status == 200) {
                        resolve(req.response);
                    }
                    else {
                        reject(Error(req.statusText));
                    }
                };
                req.onerror = function() {
                    reject(Error("Something went wrong ... "));
                };
                req.send(data);
            });
        }
        this.makeRequest = makeRequest;
    }

    window.$http = new Http();

})(window);
