(function () {
    'use strict';

    document.getElementById("term").addEventListener("keyup", findLocation, false);
    var list = document.getElementById('list');


    //Search for places as user types
    function findLocation(e){
        var keyCode = e.keyCode || e.which,
            term = this.value.toLowerCase()
            ;

        //Wait for atleast 2 charcters before searching
        if(term.length > 2){

            getLocationFromAPI('data.php?city='+term)
                .then(
                    function (response) {
                        if(response != -1)
                            displayList(response);
                    }, function (error) {
                        console.log('Unable to fetch data from API!');
                    }
            );//then


        }

        if(term.length == 0)
            list.innerHTML = '';

        return false;
    }//findLocation

    //display suggestions
    function displayList(data){

        //parse JSON data
        data = JSON.parse(data);

        //Create list elements
        var newUl = document.createElement('ul'),
            newLI;

        //Reset the list
        list.innerHTML = '';

        for(var i = 0; i < data.length; i++){
            newLI = document.createElement('li');
            newLI.innerHTML = data[i]['city']+', '+data[i]['state']+', '+data[i]['country'];
            newUl.appendChild(newLI);
        }

        list.appendChild(newUl);

    }//displayList

    //Make API call to search for location
    function getLocationFromAPI(url, data){

        return $http.makeRequest('GET', url, data);

    }//getLocationFromAPI

})();